import 'package:bloc/bloc.dart';
import 'package:btc_demo_app/data/service/btc_service.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'price_item_state.dart';

const symbol = "BTCUSDT";

class PriceItemCubit extends Cubit<PriceItemState> {

  final BtcService service;

  PriceItemCubit(this.service) : super(PriceItemInitial());

  void onItemClick() async {
    if (state is PriceItemInitial) {
      await _processedLoad();
    } else if (state is PriceItemLoadSuccess || state is PriceItemLoadFailed) {
      emit(PriceItemInitial());
    }
  }

  Future<void> _processedLoad() async {
    emit(PriceItemLoadInProgress());
    try {
      final prices = await service.getPrices();
      final result = prices.firstWhere((element) => element.symbol == symbol);
      emit(PriceItemLoadSuccess(result.lastPrice));
    } catch (e) {
      print(e);
      emit(PriceItemLoadFailed());
    }
  }
}