part of 'price_item_cubit.dart';

abstract class PriceItemState extends Equatable {
  const PriceItemState();
}

class PriceItemInitial extends PriceItemState {
  @override
  List<Object> get props => [];
}

class PriceItemLoadInProgress extends PriceItemState {
  @override
  List<Object> get props => [];
}

class PriceItemLoadSuccess extends PriceItemState {

  final String btcPrice;

  PriceItemLoadSuccess(this.btcPrice);

  @override
  List<Object> get props => [ btcPrice ];
}

class PriceItemLoadFailed extends PriceItemState {

  PriceItemLoadFailed();

  @override
  List<Object> get props => [];
}