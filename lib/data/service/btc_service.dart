import 'package:btc_demo_app/data/dto/currency.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'btc_service.g.dart';

@RestApi(baseUrl: "https://api.binance.com/api/v3")
abstract class BtcService {
  factory BtcService(Dio dio) = _BtcService;

  static BtcService of(BuildContext context) {
    return RepositoryProvider.of<BtcService>(context);
  }

  @GET("/ticker/24hr")
  Future<List<Currency>> getPrices();
}
