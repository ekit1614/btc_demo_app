import 'package:btc_demo_app/data/service/btc_service.dart';
import 'package:btc_demo_app/presentation/screen/main_screen.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (_) {
        final dio = Dio();
        return BtcService(dio);
      },
      child: MaterialApp(
        title: 'Btc demo app',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MainScreen(),
      ),
    );
  }
}
