import 'package:btc_demo_app/bloc/price_item_cubit.dart';
import 'package:btc_demo_app/data/service/btc_service.dart';
import 'package:btc_demo_app/presentation/widget/price_item.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  static final containerRadius = BorderRadius.circular(10);
  static final containerWidth = 100.0;
  static final containerHeight = 200.0;
  static final listItemSeparatorHeight = 8.0;
  static final listItemCount = 500;

  const MainScreen({Key? key}) : super(key: key);

  Widget _separatorBuilder(BuildContext context, int index) {
    return SizedBox(
      height: listItemSeparatorHeight,
    );
  }

  Widget _itemBuilder(BuildContext context, int index) {
    return PriceItem(
      cubit: PriceItemCubit(
        BtcService.of(context),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Container(
            width: containerWidth,
            height: containerHeight,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: containerRadius,
            ),
            child: ListView.separated(
              padding: EdgeInsets.all(listItemSeparatorHeight),
              itemBuilder: _itemBuilder,
              separatorBuilder: _separatorBuilder,
              itemCount: listItemCount,
            ),
          ),
        ),
      ),
    );
  }
}
