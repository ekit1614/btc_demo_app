import 'package:btc_demo_app/bloc/price_item_cubit.dart';
import 'package:btc_demo_app/res/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PriceItem extends StatelessWidget {
  static final progressSize = 20.0;
  static final btnPadding = EdgeInsets.all(10);
  static final btnRadius = BorderRadius.circular(5);

  final PriceItemCubit cubit;

  const PriceItem({
    Key? key,
    required this.cubit,
  }) : super(key: key);

  Widget _blocBuilder(BuildContext context, PriceItemState state) {
    if (state is PriceItemLoadSuccess) {
      return Text(
        state.btcPrice,
        style: TextStyle(
          color: Colors.white,
        ),
      );
    } else if (state is PriceItemLoadInProgress) {
      return SizedBox(
        width: progressSize,
        height: progressSize,
        child: CircularProgressIndicator(
          color: Colors.white,
        ),
      );
    } else if (state is PriceItemLoadFailed) {
      return Icon(
        Icons.error,
        color: Colors.white,
      );
    } else {
      return Text(
        AppStrings.pressMe,
        style: TextStyle(
          color: Colors.white,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: cubit.onItemClick,
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: btnRadius,
          color: Colors.blueGrey,
        ),
        padding: btnPadding,
        alignment: Alignment.center,
        child: BlocBuilder<PriceItemCubit, PriceItemState>(
          bloc: cubit,
          builder: _blocBuilder,
        ),
      ),
    );
  }
}
